# Avenue Code test:

## Instructions for running the project:

* 1) yarn add create-react-app
* 2) clone the project
* 3) yarn for downloading the dependencies
* 4) yarn start for starting the project 


## PROJECT AVAILABLE ONLINE (100% responsive): http://avenuecode-test.surge.sh

### TECNOLOGIES USED
* MOBX - Strightforward state manager and easy implementations
* BULMA - CSS framework with useful components
* React Google Maps - Previous knowledge of the tool and easy implementation
* AXIOS - Used for async calls, preious knowledge of the tools



