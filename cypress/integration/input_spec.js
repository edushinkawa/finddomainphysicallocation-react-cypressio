const URL = 'www.avenuecode.com'
describe('When input the search params', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })
  it('Invalid URL message appears only when type the address incorrectly', () => {
    cy
      .get('input')
      .type('www')
      .get('#invalidUrlError')
      .should('contain', 'Invalid URL')
  })
  it('Valid URL message should not display error message', () => {
    cy
      .get('input')
      .clear()
      .type(URL)
      .get('#invalidUrlError')
      .should('be.hidden')
  })
  it('Button should be disabled when searching', () => {
    cy
      .get('input')
      .clear()
      .type(URL)
      .type('{enter}')
      .get('a')
      .should('have.class', 'button is-link is-loading disabled')
  })
})

describe('When display the results', () => {
  it('Displays the correct search parameters', () => {
    cy
      .get('input')
      .clear()
      .type(URL)
      .type('{enter}')
      .get('h1')
      .should('contain', URL)
  })
  it('Displays the correct latitude and longitude', () => {
    cy
      .get('input')
      .clear()
      .type(URL)
      .type('{enter}')
      .get('h3')
      .should('contain', 'Latitude')
      .and('contain', 'Longitude')
  })
  it('Displays the map', () => {
    cy
      .get('input')
      .clear()
      .type(URL)
      .type('{enter}')
      .get('div')
      .should('have.class', 'card-content-gmap')
  })
})
