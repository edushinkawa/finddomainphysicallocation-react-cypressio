import { extendObservable, action } from 'mobx'
import axios from 'axios'

class MapStore {
  constructor() {
    extendObservable(this, {
      searchParam: '',
      addressInfo: {},
      searchOn: false,
      getAddress: action(this.getAddress),
      reset: action(this.reset),
      lat: '',
      lng: '',
      loader: false,
      urlValid: true,
      region_name: '',
      country_name: ''
    })
  }

  onChangeInput(e) {
    this.searchParam = e.target.value
    this.searchOn = false
    if(this.isUrlValid(e.target.value)){
      this.urlValid = true
    } else {
      this.urlValid = false
    }
  }

  reset() {
    this.searchOn = false
    this.loader = false
    this.searchParam = ''
    this.lat = ''
    this.lng = ''
    this.urlValid = true
  }

  getAddress(address) {
    const self = this
    this.loader = true
    this.searchOn = false
    axios.get(`http://freegeoip.net/json/${address}`).then(function(response) {
      self.addressInfo = response.data
      self.searchOn = true
      self.loader = false
      self.lat = response.data.latitude
      self.lng = response.data.longitude
      self.region_name = response.data.region_name
      self.country_name = response.data.country_name
    })
    .catch(function(error){
      self.reset()
      alert('Page not found')
    })
  }

  isUrlValid(userInput) {
    var res = userInput.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
    )
    if (res == null) return false
    else return true
  }
}

export default new MapStore()
