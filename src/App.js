import React, { Component } from 'react'
import Search from './components/search'
import Map from './components/map'
import AddressInfo from './components/addressInfo'
import './App.css'
import { observer, inject } from 'mobx-react'

class App extends Component {
  render() {
    const { map } = this.props
    return (
      <section className="container map is-fluid">
        <div className="column is-half">
          <Search />
          {map.searchOn ? (
            <div className="card">
              <div className="card-content">
                <AddressInfo />
              </div>{' '}
              <div className="card-content">
                <Map />
              </div>
            </div>
          ) : null}
        </div>
      </section>
    )
  }
}

export default inject('map')(observer(App))
