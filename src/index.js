import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'bulma/css/bulma.css'
import './index.css'
import mapStore from './stores/mapStore'
import { Provider } from 'mobx-react'

const map = mapStore
const stores = { map }

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById('root')
)
