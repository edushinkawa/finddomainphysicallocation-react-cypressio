import React, { Component } from 'react'
import 'bulma/css/bulma.css'
import { observer, inject } from 'mobx-react'

class Search extends Component {
  render() {
    const { map } = this.props
    const validadtionUrlStyle = {
      marginLeft: 13,
      color: 'red',
      fontSize: 12,
      marginTop: '-8px'
    }
    return (
      <section className="card zipcode is-9">
        <div className="card-content column is-9">
          <p>Find Latitude and longitude by address</p>
          <div className="searchField">
            <input
              className="input"
              type="text"
              placeholder="Locate"
              onChange={e => map.onChangeInput(e)}
              value={map.searchParam}
              onKeyPress={event => {
                if (event.key === 'Enter') {
                  map.getAddress(map.searchParam)
                }
              }}
              autoFocus
            />
            <a
              className={!map.loader ? 'button is-link' : 'button is-link is-loading disabled'}
              onClick={() => map.getAddress(map.searchParam)}
            >
              Locate
            </a>
          </div>
          <p style={validadtionUrlStyle} id='invalidUrlError'>{!map.urlValid && 'Invalid URL'}</p>
        </div>
      </section>
    )
  }
}

export default inject('map')(observer(Search))
