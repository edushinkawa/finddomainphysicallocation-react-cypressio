import React, { Component } from 'react'
import 'bulma/css/bulma.css'
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import { observer, inject } from 'mobx-react'
import withScriptjs from 'react-google-maps/lib/withScriptjs'

class Map extends Component {
  constructor(props) {
    super(props)
    this.state = {
      center: {
        lat: this.props.lat,
        lng: this.props.lng
      },
      markers: [
        {
          position: {
            lat: this.props.lat,
            lng: this.props.lng
          },
          defaultAnimation: 2
        }
      ]
    }
  }

  render() {
    const { map } = this.props

    const AsyncGettingStartedExampleGoogleMap = withScriptjs(
      withGoogleMap(props => (
        <GoogleMap
          defaultZoom={12}
          defaultCenter={{ lat: map.lat, lng: map.lng }}
        >
          <Marker defaultPosition={{ lat: map.lat, lng: map.lng }} />
        </GoogleMap>
      ))
    )

    return (
      <section className="card map">
        <div className="card-content-gmap">
          <AsyncGettingStartedExampleGoogleMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp"
            loadingElement={
              <div style={{ height: `100%` }}>
                <div
                  style={{
                    display: `block`,
                    width: `80px`,
                    height: `80px`,
                    margin: `150px auto`,
                    animation: `fa-spin 2s infinite linear`
                  }}
                />
              </div>
            }
            containerElement={
              <div
                style={{
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  alignItems: 'center',
                  height: '100%'
                }}
              />
            }
            mapElement={
              <div
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0
                }}
              />
            }
            center={this.state.center}
            markers={this.state.markers}
          />
        </div>
      </section>
    )
  }
}

export default inject('map')(observer(Map))
