import React, { Component } from 'react'
import 'bulma/css/bulma.css'
import { observer, inject } from 'mobx-react'

class AddressInfo extends Component {
  render() {
    const { map } = this.props

    return (
      <section className="map">
        <div className="card-content-map">
          <div className="addressInfo">
            <h1>{map.searchParam}</h1>
            <h3>Latitude: {map.lat}</h3>
            <h3>Longitude: {map.lng}</h3>
            <h2>
              The physical location of <strong>{map.searchParam}</strong> is in {map.region_name && `${map.region_name},`} {map.country_name}.
            </h2>
          </div>
          <div className="close">
            <a class="button is-white" onClick={() => map.reset()}>
              X
            </a>
          </div>
        </div>
      </section>
    )
  }
}

export default inject('map')(observer(AddressInfo))
